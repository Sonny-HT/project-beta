import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import ManufacturersList from "./components/ManufacturersList";
import NewManufacturer from "./components/NewManufacturer";
import ModelLists from "./components/ModelList";
import NewModel from "./components/NewModel";
import AutomobileList from "./components/AutomobileList";
import NewAutomobile from "./components/NewAutomobile";
import TechnicianList from "./components/services/TechnicianList";
import NewTechnician from "./components/services/NewTechnician";
import NewAppointment from "./components/services/NewAppointment";
import AppointmentList from "./components/services/AppointmentList";
import ServiceHistory from "./components/services/ServiceHistory";
import CustomerForm from "./components/sales/Addcustomerform";
import SalesPersonForm from "./components/sales/AddSalesperson";
import ListCustomers from "./components/sales/ListCustomers";
import Listsalespeople from "./components/sales/ListSalespeople";
import SaleForm from "./components/sales/AddSaleForm";
import ListSale from "./components/sales/ListSales";
import SalespersonHistory from "./components/sales/SalespersonHistory";
import Nav from "./Nav";

function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="manufacturers">
						<Route path="" element={<ManufacturersList />} />
						<Route path="new" element={<NewManufacturer />} />
					</Route>
					<Route path="models">
						<Route path="" element={<ModelLists />} />
						<Route path="new" element={<NewModel />} />
					</Route>
					<Route path="automobiles">
						<Route path="" element={<AutomobileList />} />
						<Route path="new" element={<NewAutomobile />} />
					</Route>
					<Route path="technicians">
						<Route path="" element={<TechnicianList />} />
						<Route path="new" element={<NewTechnician />} />
					</Route>
					<Route path="appointments">
						<Route path="" element={<AppointmentList />} />
						<Route path="new" element={<NewAppointment />} />
						<Route path="history" element={<ServiceHistory />} />
					</Route>
					<Route path="salespeople">
						<Route path="new" element={<SalesPersonForm />} />
						<Route path="" element={<Listsalespeople/>}/>
					</Route>
					<Route path="customers">
						<Route path="new" element={<CustomerForm />}/>
						<Route path="" element={<ListCustomers/>}/>
					</Route>
					<Route path="sales">
						<Route path="create" element={<SaleForm/>}/>
						<Route path="" element={<ListSale/>}/>
						<Route path="history" element={<SalespersonHistory/>}/>
					</Route>
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
