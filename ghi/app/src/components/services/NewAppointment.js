import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function NewAppointment() {
	const navigate = useNavigate();

	const [customers, setCustomers] = useState([]);
	const [technicians, setTechnicians] = useState([]);
	const [formData, setFormData] = useState({
		technician: "",
		customer: "",
		date: "",
		time: "",
		reason: "",
		vin: "",
	});

	const fetchData = async () => {
		const response = await fetch("http://localhost:8080/api/technicians");
		const data = await response.json();
		setTechnicians(data.technicians);
	};

	const fetchCustomers = async () => {
		const response = await fetch("http://localhost:8090/api/customers");
		const data = await response.json();
		setCustomers(data.customers);
	};

	useEffect(() => {
		fetchData();
		fetchCustomers();
	}, []);

	const handleChange = (event) => {
		setFormData({ ...formData, [event.target.name]: event.target.value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const combinedDateTime = new Date(`${formData.date}T${formData.time}`);
		const formattedDateTime = combinedDateTime.toISOString();
		const newForm = {
			technician: formData.technician,
			customer: formData.customer,
			date_time: formattedDateTime,
			reason: formData.reason,
			vin: formData.vin,
		};
		const url = "http://localhost:8080/api/appointments/";
		console.log(JSON.stringify(newForm));
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(newForm),
			headers: {
				"Content-Type": "application/json",
			},
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			setFormData({
				technician: "",
				customer: "",
				date: "",
				time: "",
				reason: "",
				vin: "",
			});
			navigate("/appointments");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h4>Add an Appointment</h4>
					<form id="create-appointment-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								placeholder="vin"
								required
								type="text"
								name="vin"
								id="vin"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="vin">Automobile VIN</label>
						</div>
						<div className="mb-3">
							<select
								className="form-select"
								name="customer"
								id="customer"
								onChange={handleChange}
							>
								<option value="">Select a Customer</option>
								{customers.map((customer) => (
									<option
										key={
											customer.first_name +
											" " +
											customer.last_name
										}
										value={
											customer.first_name +
											" " +
											customer.last_name
										}
									>
										{customer.first_name}{" "}
										{customer.last_name}
									</option>
								))}
							</select>
						</div>
						<div className="form-floating mb-3">
							<input
								placeholder="date"
								required
								type="date"
								name="date"
								id="date"
								className="form-control"
								onChange={handleChange}
							></input>
						</div>
						<div className="form-floating mb-3">
							<input
								placeholder="time"
								required
								type="time"
								name="time"
								id="time"
								className="form-control"
								onChange={handleChange}
							></input>
						</div>
						<div className="mb-3">
							<select
								className="form-select"
								name="technician"
								id="technician"
								onChange={handleChange}
							>
								<option value="">Select a Technician</option>
								{technicians.map((technician) => (
									<option
										key={technician.id}
										value={technician.id}
									>
										{technician.first_name}{" "}
										{technician.last_name}
									</option>
								))}
							</select>
						</div>
						<div className="form-floating mb-3">
							<input
								placeholder="reason"
								required
								name="reason"
								id="reason"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="reason">
								Reason for Appointment
							</label>
						</div>
						<button type="submit" className="btn btn-primary">
							Create
						</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewAppointment;
