import React, { useState, useEffect } from "react";

function AppointmentList() {
	const [appointments, setAppointments] = useState([]);
	const [automobileVINs, setAutomobileVINs] = useState([]);

	const fetchData = async () => {
		const response = await fetch("http://localhost:8080/api/appointments/");
		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments);
		}
	};

	const fetchAutomobiles = async () => {
		const response = await fetch("http://localhost:8080/api/automobiles/");
		if (response.ok) {
			const data = await response.json();
			const automobiles = data.automobileVOs;
			const VINs = automobiles.map((automobile) => {
				return automobile.vin;
			});
			setAutomobileVINs(VINs);
		}
	};

	useEffect(() => {
		fetchData();
		fetchAutomobiles();
	}, []);

	const handleCancel = async (event) => {
		const url = `http://localhost:8080/api/appointments/${event.target.id}/cancel/`;
		const fetchConfig = {
			method: "PUT",
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchData();
		}
	};

	const handleFinish = async (event) => {
		const url = `http://localhost:8080/api/appointments/${event.target.id}/finish/`;
		const fetchConfig = {
			method: "PUT",
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchData();
		}
	};

	return (
		<React.Fragment>
			<div className="container-fluid">
				<h1>Service Appointments</h1>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>VIN</th>
							<th>VIP?</th>
							<th>Customer</th>
							<th>Date</th>
							<th>Time</th>
							<th>Technician</th>
							<th>Reason</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						{appointments.map((appointment) => {
							if (appointment.status === "created") {
								const date = new Date(appointment.date_time);
								const options = {
									year: "numeric",
									month: "2-digit",
									day: "2-digit",
								};
								const timeOptions = {
									hour: "2-digit",
									minute: "2-digit",
								};

								let VIP = "No";
								if (automobileVINs.includes(appointment.vin)) {
									VIP = "Yes";
								}

								return (
									<tr key={appointment.id}>
										<td>{appointment.vin}</td>
										<td>{VIP}</td>
										<td>{appointment.customer}</td>
										<td>
											{date.toLocaleString(
												"en-US",
												options
											)}
										</td>
										<td>
											{date.toLocaleTimeString(
												[],
												timeOptions
											)}
										</td>
										<td>
											{appointment.technician.first_name}{" "}
											{appointment.technician.last_name}
										</td>
										<td>{appointment.reason}</td>
										<td>
											<button
												id={appointment.id}
												className="btn btn-success"
												onClick={handleFinish}
											>
												Finish
											</button>
											<button
												id={appointment.id}
												className="btn btn-danger"
												onClick={handleCancel}
											>
												Cancel
											</button>
										</td>
									</tr>
								);
							}
						})}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
}

export default AppointmentList;
