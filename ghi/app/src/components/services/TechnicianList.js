import React, { useEffect, useState } from "react";

function TechnicianList() {
	const [technicians, setTechnicians] = useState([]);

	const fetchData = async () => {
		const response = await fetch("http://localhost:8080/api/technicians/");
		if (response.ok) {
			const data = await response.json();
			setTechnicians(data.technicians);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	const handleDelete = async (event) => {
		const url = `http://localhost:8080/api/technicians/${event.target.id}/`;
		const fetchConfig = {
			method: "DELETE",
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchData();
		}
	};

	return (
		<React.Fragment>
			<div className="container-fluid">
				<h1>Technicians</h1>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Employee ID</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Delete?</th>
						</tr>
					</thead>
					<tbody>
						{technicians.map((technician) => {
							return (
								<tr key={technician.id}>
									<td>{technician.employee_id}</td>
									<td>{technician.first_name}</td>
									<td>{technician.last_name}</td>
									<td>
										<button
											id={technician.id}
											className="btn btn-danger"
											onClick={handleDelete}
										>
											Delete
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
}

export default TechnicianList;
