import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function NewTechnician() {
	const navigate = useNavigate();

	const [formData, setFormData] = useState({
		employee_id: "",
		first_name: "",
		last_name: "",
	});

	const handleChange = (event) => {
		setFormData({ ...formData, [event.target.name]: event.target.value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const url = "http://localhost:8080/api/technicians/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "applicaton/json",
			},
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			setFormData({
				employee_id: "",
				first_name: "",
				last_name: "",
			});
			navigate("/technicians");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h4>Add a Technician</h4>
					<form id="create-technician-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								placeholder="Employee ID"
								required
								type="text"
								name="employee_id"
								id="employee_id"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="employee_id">Employee ID...</label>
						</div>
						<div className="form-floating mb-3">
							<input
								placeholder="First Name"
								required
								type="text"
								name="first_name"
								id="first_name"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="first_name">First Name...</label>
						</div>
						<div className="form-floating mb-3">
							<input
								placeholder="Last Name"
								required
								type="text"
								name="last_name"
								id="last_name"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="last_name">Last Name...</label>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewTechnician;
