import React, { useEffect, useState } from "react";

function ServiceHistory() {
	const [appointments, setAppointments] = useState([]);
	const [searchField, setSearchField] = useState("");
	const [searchTerm, setSearchTerm] = useState("");
	const [automobileVINs, setAutomobileVINs] = useState([]);

	const fetchData = async () => {
		const response = await fetch("http://localhost:8080/api/appointments/");
		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments);
		}
	};

	const fetchAutomobiles = async () => {
		const response = await fetch("http://localhost:8080/api/automobiles/");
		if (response.ok) {
			const data = await response.json();
			const automobiles = data.automobileVOs;
			const VINs = automobiles.map((automobile) => {
				return automobile.vin;
			});
			setAutomobileVINs(VINs);
		}
	};

	useEffect(() => {
		fetchData();
		fetchAutomobiles();
	}, []);

	return (
		<React.Fragment>
			<div className="container-fluid">
				<h1>Service History</h1>
				<div className="input-group mb-3">
					<input
						type="text"
						className="form-control"
						placeholder="Search by VIN..."
						aria-label="Search"
						aria-describedby="button-addon2"
						onChange={(event) => {
							setSearchField(event.target.value);
						}}
					/>
					<div className="input-group-append">
						<button
							className="btn btn-outline-secondary"
							type="button"
							onClick={() => {
								setSearchTerm(searchField);
							}}
						>
							Search
						</button>
					</div>
				</div>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>VIN</th>
							<th>Is VIP?</th>
							<th>Customer</th>
							<th>Date</th>
							<th>Time</th>
							<th>Technician</th>
							<th>Reason</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						{appointments.map((appointment) => {
							const date = new Date(appointment.date_time);

							let VIP = "No";
							if (automobileVINs.includes(appointment.vin)) {
								VIP = "Yes";
							}

							const options = {
								year: "numeric",
								month: "2-digit",
								day: "2-digit",
							};
							const timeOptions = {
								hour: "2-digit",
								minute: "2-digit",
							};

							if (appointment.vin.includes(searchTerm)) {
								return (
									<tr
										key={appointment.id}
										value={appointment.vin}
									>
										<td>{appointment.vin}</td>
										<td>{VIP}</td>
										<td>{appointment.customer}</td>
										<td>
											{date.toLocaleDateString(
												"en-US",
												options
											)}
										</td>
										<td>
											{date.toLocaleTimeString(
												"en-US",
												timeOptions
											)}
										</td>
										<td>
											{appointment.technician.first_name}{" "}
											{appointment.technician.last_name}
										</td>
										<td>{appointment.reason}</td>
										<td>{appointment.status}</td>
									</tr>
								);
							}
						})}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
}

export default ServiceHistory;
