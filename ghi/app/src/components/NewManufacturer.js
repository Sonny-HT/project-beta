import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function NewManufacturer() {
	const navigate = useNavigate();

	const [formData, setFormData] = useState({
		name: "",
	});

	const handleChange = (event) => {
		setFormData({ ...formData, [event.target.name]: event.target.value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const url = "http://localhost:8100/api/manufacturers/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "applicaton/json",
			},
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			setFormData({
				name: "",
			});
			navigate("/manufacturers");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h4>Create a Manufacturer</h4>
					<form id="create-manufacturer-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								placeholder="Manufacturer Name"
								required
								type="text"
								name="name"
								id="name"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="name">Manufacturer name...</label>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewManufacturer;
