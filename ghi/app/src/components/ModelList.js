import React, { useState, useEffect } from "react";

function ModelLists() {
	const [models, setModels] = useState([]);

	const fetchData = async () => {
		const response = await fetch("http://localhost:8100/api/models/");
		if (response.ok) {
			const data = await response.json();
			setModels(data.models);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<React.Fragment>
			<div className="container-fluid">
				<h1>Models</h1>
				<table className="table table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Manufacturer</th>
							<th>Picture</th>
						</tr>
					</thead>
					<tbody>
						{models.map((model) => {
							return (
								<tr key={model.id}>
									<td>{model.name}</td>
									<td>{model.manufacturer.name}</td>
									<td>
										<img
											src={model.picture_url}
											alt={model.name}
											width="300px"
											height="200px"
										/>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
}

export default ModelLists;
