import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function NewModel() {
	const navigate = useNavigate();

	const [manufacturers, setManufacturers] = useState([]);

	const [formData, setFormData] = useState({
		name: "",
		manufacturer_id: "",
		picture_url: "",
	});

	const fetchData = async () => {
		const response = await fetch(
			"http://localhost:8100/api/manufacturers/"
		);
		if (response.ok) {
			const data = await response.json();
			setManufacturers(data.manufacturers);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	const handleChange = (event) => {
		setFormData({ ...formData, [event.target.name]: event.target.value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const url = "http://localhost:8100/api/models/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "applicaton/json",
			},
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			setFormData({
				name: "",
				manufacturer_id: "",
				picture_url: "",
			});
			navigate("/models");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h4>Create a Model</h4>
					<form id="create-model-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								placeholder="Model Name"
								required
								type="text"
								name="name"
								id="name"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="name">Model name...</label>
						</div>
						<div className="form-floating mb-3">
							<input
								placeholder="picture_url"
								required
								type="text"
								name="picture_url"
								id="picture_url"
								className="form-control"
								onChange={handleChange}
							></input>
							<label htmlFor="picture_url">Picture URL...</label>
						</div>
						<div className="mb-3">
							<select
								required
								name="manufacturer_id"
								id="manufacturer_id"
								className="form-select"
								onChange={handleChange}
							>
								<option value="">
									Select a manufacturer...
								</option>
								{manufacturers.map((manufacturer) => {
									return (
										<option
											key={manufacturer.id}
											value={manufacturer.id}
										>
											{manufacturer.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewModel;
