import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";


function SalespersonForm() {
    const [formData, setFormData] = useState({
		first_name: "",
		last_name: "",
        employee_id: "",
	});

    const navigate = useNavigate();


    const handleformChange = (event) => {
        setFormData({ ...formData, [event.target.name]: event.target.value });
    };

    const handleformSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "applicaton/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        console.log(formData)
        if (response.ok) {
            setFormData({
                employee_id: "",
                first_name: "",
                last_name: "",
            });
            navigate("/salespeople")
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleformSubmit} id="SalespersonForm">
                    <h1>Add a Salesperson</h1>
                        <div className="form-floating mb-3">
                            <input onChange={handleformChange}
                            placeholder="first name" required
                            type="text"
                            id="first_name"
                            name="first_name"
                            className="form-control" />
                            <label htmlFor="first_name">First Name... </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleformChange}
                                placeholder="last name" required
                                type="text"
                                name="last_name"
                                id="last_name"
                                className="form-control"
                            />
                            <label htmlFor="last_name">Last Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleformChange}
                                placeholder="employee id" required
                                type="text"
                                id="employee_id"
                                name="employee_id"
                                className="form-control"
                            />
                            <label htmlFor="employee_id">Employee Id...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default SalespersonForm;
