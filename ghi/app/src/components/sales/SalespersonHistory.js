import React, {useState, useEffect } from "react";
function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState([]);
    const fetchData = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salesperson);
        }
        const salesURL = "http://localhost:8090/api/sales/"
        const Response = await fetch(salesURL);
        if (Response.ok) {
            const data = await Response.json();
            setSales(data.sales);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <React.Fragment>
            <div className="container-fluid">
                <h1>Salespeople History</h1>
                <select className="form-select"
                name="salesperson"
                id="salesperson">
                <option value="">Select a Salesperson</option>
                {salesperson.map((salesperson) => (
                    <option
                    key= {salesperson.id}
                    value={salesperson.id}>
                    {salesperson.first_name}{" "} {salesperson.last_name}
                    </option>
                    ))}
                </select>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale) => {
                        return (
                        <tr key={sale.id} value={sale.id}>
                            <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                        );
                        })}
                </tbody>
            </table>
        </div>
    </React.Fragment>
    );
}
export default SalespersonHistory;
