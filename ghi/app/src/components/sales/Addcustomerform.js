import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";

function CustomerForm() {
    const [formData, setFormData] = useState({
		first_name: "",
		last_name: "",
        phone_number: "",
        address: "",
	});

    const navigate = useNavigate();


    const handleformChange = (event) => {
        setFormData({ ...formData, [event.target.name]: event.target.value });
    };

    const handleformSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "applicaton/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                employee_id: "",
                first_name: "",
                phone_number: "",
                address: "",
            });
            navigate("/customers")
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleformSubmit} id="CustomerForm">
                    <h1>Add a Customer</h1>
                        <div className="form-floating mb-3">
                            <input onChange={handleformChange}
                            placeholder="first name" required
                            type="text"
                            id="first_name"
                            name="first_name"
                            className="form-control" />
                            <label htmlFor="first_name">First Name... </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleformChange}
                                placeholder="last name" required
                                type="text"
                                id="last_name"
                                name="last_name"
                                className="form-control"
                            />
                            <label htmlFor="last_name">Last Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleformChange}
                                placeholder="phone number" required
                                type="text"
                                id="phone_number"
                                name="phone_number"
                                className="form-control"
                            />
                            <label htmlFor="phone_number">Phone Number...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleformChange}
                                placeholder="address" required
                                type="text"
                                id="address"
                                name="address"
                                className="form-control"
                            />
                            <label htmlFor="address">Address...</label>
                        </div>
                        <button className="btn btn-primary">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default CustomerForm;
