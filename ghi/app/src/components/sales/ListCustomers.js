import React, { useEffect, useState } from "react";

function ListCustomers() {
	const [customers, setCustomers] = useState([]);

	const fetchData = async () => {
		const response = await fetch("http://localhost:8090/api/customers/");
		if (response.ok) {
			const data = await response.json();
			setCustomers(data.customers);
		}
	};

    useEffect(() => {
        fetchData();
    }, []);

	const handleClick = async (event) => {
		const url = `http://localhost:8090/api/customers/${event.target.id}/`;
		const fetchConfig = {
			method: "DELETE",
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchData();
		}
	};

    return (
		<React.Fragment>
			<div className="container-fluid">
				<h1>Customers</h1>
				<table className="table table-striped">
                    <thead>
                        <tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Phone Number</th>
							<th>Address</th>
                        </tr>
                    </thead>
					<tbody>
						{customers.map((customer) => {
							return (
								<tr key= {customer.id}>
									<td>{customer.first_name}</td>
									<td>{customer.last_name}</td>
									<td>{customer.phone_number}</td>
									<td>{customer.address}</td>
									<td>
										<button
											className="btn btn-danger"
											id={customer.id}
											onClick={handleClick}> Delete
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
};
export default ListCustomers
