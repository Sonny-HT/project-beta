import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function SaleForm() {
    const[salesperson, setSalesperson] = useState([]);
    const[customers, setCustomers] = useState([]);
    const[automobiles, setAutomobiles] = useState([])
    const[formData, setFormData]= useState({
        salesperson: "",
        customer:"",
        automobile:"",
        price: "",
    });

    const navigate = useNavigate()

    const handleformChange = (event) => {
        setFormData({ ...formData, [event.target.name]: event.target.value });
    };

    const handleformSubmit = async (event) => {
        event.preventDefault();
        console.log(formData)
        const url = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "applicaton/json",
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                salesperson: "",
                customer: "",
                automobile: "",
                price:"",
            });
            navigate("/sales")
        }
    };

    const fetchSalesperson = async()=> {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salesperson)
        }
    }

    const fetchCustomer = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
    }

    const fetchAutomobiles = async () => {
        const url ="http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }

    useEffect(() => {
        fetchCustomer();
        fetchSalesperson();
        fetchAutomobiles();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleformSubmit} id="SaleForm">
                    <h1>Register a new sale</h1>
                        <div className="mb-3">
                            <select className="form-select"
                            name="automobile"
                            id="automobile"
                            onChange={handleformChange}>
                            <option value=""> Select an Automobile</option>
                            {automobiles.map(automobile => {
                                return (
                                <option key= {automobile.vin} value={automobile.vin}>
                                    {automobile.vin}
                                </option>)})}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select
								className="form-select"
								name="salesperson"
								id="salesperson"
								onChange={handleformChange}>
								<option value="">Select a Salesperson</option>
								{salesperson.map((salesperson) => (
									<option
										key= {salesperson.id}
										value={salesperson.id}>
										{salesperson.first_name}{" "} {salesperson.last_name}
									</option>
								))}
							</select>
                        </div>
                        <div className="form-floating mb-3">
                            <select
								className="form-select"
								name="customer"
								id="customer"
								onChange={handleformChange}>
								<option value="">Select a Customer</option>
								{customers.map((customer) => (
									<option
										key={customer.id}
										value={customer.id}>
										{customer.first_name}{" "} {customer.last_name}
									</option>
								))}
							</select>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleformChange}
                                placeholder="price" required
                                type="integer"
                                name="price"
                                id="price"
                                className="form-control"
                            />
                            <label htmlFor="Price">
                                Price...
                            </label>
                        </div>
                        <button className="btn btn-primary">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default SaleForm;
