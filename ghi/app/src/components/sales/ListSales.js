import React, { useEffect, useState } from "react";

function ListSale() {
	const [sales, setSale] = useState([]);

	const fetchData = async () => {
		const response = await fetch("http://localhost:8090/api/sales/");
		if (response.ok) {
			const data = await response.json();
			setSale(data.sales);
		}
	};

    useEffect(() => {
        fetchData();
    }, []);

    const handleClick = async (event) => {
		const url = `http://localhost:8090/api/sales/${event.target.id}/`;
		const fetchConfig = {
			method: "DELETE",
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchData();
		}
	};


    return (
		<React.Fragment>
			<div className="container-fluid">
				<h1>Sales</h1>
				<table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson Employee ID</th>
							<th>Salesperson Name</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
					<tbody>
						{sales.map((sale) => {
							return (
								<tr key={sale.id}>
									<td>{sale.salesperson.employee_id}</td>
									<td>{sale.salesperson.first_name + "" + sale.salesperson.last_name}</td>
                                    <td>{sale.customer.first_name + "" + sale.customer.last_name}</td>
									<td>{sale.automobile.vin}</td>
                                    <td>{sale.price}</td>
									<td>
										<button
											className="btn btn-danger"
											id={sale.id}
											onClick={handleClick}> Delete
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
};
export default ListSale
