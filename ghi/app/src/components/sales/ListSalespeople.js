import React, { useEffect, useState } from "react";

function Listsalespeople() {
	const [salesperson, setSalesperson] = useState([]);

	const fetchData = async () => {
		const response = await fetch("http://localhost:8090/api/salespeople/");
		if (response.ok) {
			const data = await response.json();
			setSalesperson(data.salesperson);
		}
	};

    useEffect(() => {
        fetchData();
    }, []);

    const handleClick = async (event) => {
		const url = `http://localhost:8090/api/salespeople/${event.target.id}/`;
		const fetchConfig = {
			method: "DELETE",
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			fetchData();
		}
	};

    return (
		<React.Fragment>
			<div className="container-fluid">
				<h1>Salespeople</h1>
				<table className="table table-striped">
                    <thead>
                        <tr>
                            <th>First Name</th>
							<th>Last Name</th>
                            <th>Employee ID</th>
                        </tr>
                    </thead>
					<tbody>
						{salesperson.map((salesperson) => {
							return (
								<tr key= {salesperson.id}>
									<td>{salesperson.first_name}</td>
									<td>{salesperson.last_name}</td>
									<td>{salesperson.employee_id}</td>
									<td>
										<button
											className="btn btn-danger"
											id={salesperson.id}
											onClick={handleClick}> Delete
										</button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</table>
			</div>
		</React.Fragment>
	);
};
export default Listsalespeople
