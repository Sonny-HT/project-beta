import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function NewAutomobile() {
	const navigate = useNavigate();

	const [models, setModels] = useState([]);

	const [formData, setFormData] = useState({
		vin: "",
		color: "",
		year: "",
		model_id: "",
	});

	const fetchData = async () => {
		const response = await fetch("http://localhost:8100/api/models/");
		if (response.ok) {
			const data = await response.json();
			setModels(data.models);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	const handleChange = (event) => {
		setFormData({ ...formData, [event.target.name]: event.target.value });
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const url = "http://localhost:8100/api/automobiles/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(formData),
			headers: {
				"Content-Type": "applicaton/json",
			},
		};
		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			setFormData({
				vin: "",
				color: "",
				year: "",
				model_id: "",
			});
			navigate("/automobiles");
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h4>Create an Automobile</h4>
					<form id="create-automobile-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								type="text"
								className="form-control"
								id="color"
								name="color"
								placeholder="Color"
								value={formData.color}
								onChange={handleChange}
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								type="text"
								className="form-control"
								id="year"
								name="year"
								placeholder="Year"
								value={formData.year}
								onChange={handleChange}
							/>
							<label htmlFor="year">Year</label>
						</div>
						<div className="form-floating mb-3">
							<input
								type="text"
								className="form-control"
								id="vin"
								name="vin"
								placeholder="VIN"
								value={formData.vin}
								onChange={handleChange}
							/>
							<label htmlFor="vin">VIN</label>
						</div>
						<div className="mb-3">
							<select
								className="form-select"
								id="model_id"
								name="model_id"
								value={formData.model_id}
								onChange={handleChange}
							>
								<option value="">Select a Model</option>
								{models.map((model) => {
									return (
										<option key={model.id} value={model.id}>
											{model.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewAutomobile;
