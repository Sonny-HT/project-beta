from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "id",
        "technician",
    ]
    encoders = {"technician": TechnicianListEncoder()}
