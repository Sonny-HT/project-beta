from django.db import models


# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=100, unique=True)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=300)
    status = models.CharField(max_length=300, default="created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=150)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE
    )
