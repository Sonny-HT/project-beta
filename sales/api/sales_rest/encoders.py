from common.json import ModelEncoder
from .models import Salesperson, AutomobileVO, Customer, Sale

class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]

class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "id"]

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [ "first_name", "last_name", "address", "phone_number", "id"]


class SalesEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "id"]
    encoders = {"salesperson" : SalespersonListEncoder(), 'automobile': AutomobileVOListEncoder(), "customer": CustomerListEncoder()}

    def get_extra_data(self, o):
        return { "price": str(o.price)}
