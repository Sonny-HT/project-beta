from django.contrib import admin
from .models import AutomobileVO

@admin.register(AutomobileVO)
class AutomobileVOadmin(admin.ModelAdmin):
    list_display = ["vin"]
