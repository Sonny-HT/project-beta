from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, AutomobileVO, Sale, Customer
import json
from common.json import ModelEncoder
from .encoders import AutomobileVOListEncoder, SalesEncoder, CustomerListEncoder, SalespersonListEncoder



@require_http_methods(["GET", "POST"])
def api_list_Salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(salesperson, encoder=SalespersonListEncoder, safe=False)
        except Exception as e:
            return JsonResponse(
                {"message": "Salesperson ID is not unique or a field is not valid"},
                status=400,
            )

@require_http_methods(["DELETE"])
def api_delete_salesperson(request, pk):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(salesperson, encoder=SalespersonListEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"}, status=404)


@require_http_methods(["GET", "POST"])
def api_list_Customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerListEncoder, safe=False)
        except Exception as e:
            return JsonResponse(
                {"message": "Customer id is not unique or a field is not valid"},
                status=400,
            )

@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    if request.method == "DELETE":
        try:
            customers = Customer.objects.get(id=pk)
            customers.delete()
            return JsonResponse(customers, encoder=CustomerListEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"}, status=404)

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = content["salesperson"]
            salesperson = Salesperson.objects.get(id = salesperson)
            content["salesperson"] = salesperson

            customer = content["customer"]
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
            print(content)

            automobile = AutomobileVO.objects.get(vin = content["automobile"])
            print(automobile)
            content["automobile"] = automobile

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )

        except (
            AutomobileVO.DoesNotExist,
            Salesperson.DoesNotExist,
            Customer.DoesNotExist
            ):
            response = JsonResponse(
            {"message": "Sale does not exist"},
            status=404,
        )
        return response

@require_http_methods(["DELETE"])
def api_delete_sale(request, pk):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(sale, encoder=SalesEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale does not exist"}, status=404)
