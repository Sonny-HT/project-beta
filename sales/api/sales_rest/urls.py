from .views import api_delete_customer, api_delete_salesperson, api_delete_sale, api_list_Customer, api_list_Salesperson, api_list_sales
from django.urls import path
urlpatterns = [
    path("salespeople/", api_list_Salesperson, name="api_list_salesperson"),
    path("salespeople/<int:pk>/", api_delete_salesperson, name="api_delete_salesperson"),
    path("customers/", api_list_Customer, name="api_list_Customer"),
    path("customers/<int:pk>/", api_delete_customer, name="api_delete_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_delete_sale, name="api_delete_sale"),
]
